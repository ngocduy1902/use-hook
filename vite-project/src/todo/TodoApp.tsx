import { useState } from "react";
import useTodoAppBusinessLogic from "./useTodoAppBusinessLogic"

export default function TodoApp() {
  const [txtInput, setTxtInput] = useState<string>('');

  const {
    data, createItem, deleteItem
  } = useTodoAppBusinessLogic();

  return (
    <>
      <div>
        <button
          onClick={() => createItem(txtInput)}
        >
          Tạo mới
        </button>
        <input
          value={txtInput}
          onChange={e => setTxtInput(e.target.value)}
        />
      </div>
      {data.map(item => {
        return (
          <div>
            <p>Id: {item.id}</p>
            <p>Công việc: {item.name}</p>
            <button
              onClick={() => deleteItem(item)}
            >
              Xóa
            </button>
          </div>
        )
      })}
    </>
  )
}
