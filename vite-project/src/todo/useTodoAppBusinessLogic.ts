import { useState } from "react"
import { ITodoItem } from "./model"

export default function useTodoAppBusinessLogic() {
  const [data, setData] = useState<ITodoItem[]>([]);
  
  const createItem = (name: string) => {
    const todoItem: ITodoItem = {
      id: Math.random(),
      name
    };

    // Sao chép ra mảng mới
    // Gán dấu bằng thì chỉ gán con trỏ đi
    // React phải thấy sự thay đổi giữa data trước và sau khi click thuộc nó mới RENDER
    const newData = JSON.parse(JSON.stringify(data));

    newData.push(todoItem);

    // setData -> data thay đổi -> tất cả mọi thứ dính tới data PHẢI render lại toàn bộ
    setData(newData);
  }

  const deleteItem = (item: ITodoItem) => {
    let newData = JSON.parse(JSON.stringify(data)) as ITodoItem[];

    newData = newData.filter(u => u.id != item.id);

    // setData -> data thay đổi -> tất cả mọi thứ dính tới data PHẢI render lại toàn bộ
    setData(newData);
  }

  return {
    data, // Ghi data: data cũng được, trùng tên thì nó tự hiểu
    createItem,
    deleteItem
  }
}
